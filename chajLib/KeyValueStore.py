#-------------------------------------------------------------------------------
# Name:        KeyValueStore
# Purpose:     Wrapper around a text file contains lines of key{sep}value, supporting reads, local writes, and saved flushes.
# Author:      Charles J. Daniels
# Created:     06/11/2014
# Copyright:   (c) Charles J. Daniels 2014
# Licence:     All Rights Reserved
#-------------------------------------------------------------------------------

class KeyValueStore:
    def __init__(self, filepath, separator = "="):
        self.separator = separator
        self.filepath = filepath
        self.store = {}
        self.LoadData()

    def LoadData(self):
        self.store = {}
        data = open(self.filepath, "r")
        lines = data.readlines()
        data.close()
        for line in lines:
            line = line.rstrip()
            if line.find(self.separator) == -1:
                continue
            key_value = line.split(self.separator)
            self.store[key_value[0]] = key_value[1]

    def GetInt(self, key):
        value = self.GetValue(key)
        try:
            value = int(value)
        except ValueError:
            value = None
        return value

    def GetValue(self, key):
        if key not in self.store:
            return None
        else:
            return self.store[key]

    def UpdateValue(self, key, value):
        self.store[key] = value

    def Flush(self):
        lines = []
        for key in self.store.keys():
            lines.append(key + self.separator + self.store[key] + "\n")
        data = open(self.filepath, "w")
        data.writelines(lines)
        data.close()