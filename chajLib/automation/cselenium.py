# python imports
import collections
# selenium imports
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver import ActionChains

_DEFAULT_BROWSER = None


def determine_browser_throw(trump):
    if trump is not None:
        return trump
    if _DEFAULT_BROWSER is not None:
        return _DEFAULT_BROWSER
    raise RuntimeError("Cannot determine the selenium browser instance -- use set_default_browser, or supply the argument directly.")


# decorator
def UseDefaultBrowserIfNone(browser_call):
    def browser_call_replacement(*args, **kwargs):
        browser = determine_browser_throw(kwargs.get("browser", None))
        kwargs["browser"] = browser
        browser_call(*args, **kwargs)
    return browser_call_replacement

@UseDefaultBrowserIfNone
def click_element_by_css_selector(selector, browser=None):
    element = browser.find_element_by_css_selector(selector)
    element.click()
                 
@UseDefaultBrowserIfNone
def click_element_by_id(element_id, browser=None):
    element = browser.find_element_by_id(element_id)
    element.click()


@UseDefaultBrowserIfNone
def click_element_by_name(name, browser=None):
    element = browser.find_element_by_name(name)
    element.click()


@UseDefaultBrowserIfNone
def send_keys_by_name(name, value, browser=None):
    element = browser.find_element_by_name(name)
    element.send_keys(value)


def set_default_browser(browser):
    global _DEFAULT_BROWSER
    _DEFAULT_BROWSER = browser


@UseDefaultBrowserIfNone
def set_text_input_value(name, value, browser=None):
    """Unlike send_keys, this will clear any pre-existing value."""
    element = browser.find_element_by_name(name)
    element.clear()
    element.send_keys(value)


@UseDefaultBrowserIfNone
def submit_element_by_name(name, browser=None):
    element = browser.find_element_by_name(name)
    element.submit()

@UseDefaultBrowserIfNone
def wait_until_title_contains(title_content, seconds_to_wait, browser=None):
    WebDriverWait(browser, seconds_to_wait).until(EC.title_contains(title_content))


@UseDefaultBrowserIfNone
def wait_until_body_contains(body_content, seconds_to_wait, browser=None):
    WebDriverWait(browser, seconds_to_wait).until(EC.text_to_be_present_in_element((By.TAG_NAME, 'body'), body_content))