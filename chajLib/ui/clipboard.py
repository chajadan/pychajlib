import win32clipboard

def get_clipboard_as_text():
    text = None
    try:
        win32clipboard.OpenClipboard()
        text = win32clipboard.GetClipboardData(win32clipboard.CF_TEXT)
        win32clipboard.CloseClipboard()
    except:
        pass
    return text