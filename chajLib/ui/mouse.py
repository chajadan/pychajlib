import ctypes
import time, sys
import win32api
from mouse_types import *
import screen
import emulation as emul

LONG = ctypes.c_long
DWORD = ctypes.c_ulong
ULONG_PTR = ctypes.POINTER(DWORD)
WORD = ctypes.c_ushort

WHEEL_DELTA = 120
XBUTTON1 = 0x0001
XBUTTON2 = 0x0002
MOUSEEVENTF_ABSOLUTE = 0x8000
MOUSEEVENTF_HWHEEL = 0x01000
MOUSEEVENTF_MOVE = 0x0001
MOUSEEVENTF_MOVE_NOCOALESCE = 0x2000
MOUSEEVENTF_LEFTDOWN = 0x0002
MOUSEEVENTF_LEFTUP = 0x0004
MOUSEEVENTF_RIGHTDOWN = 0x0008
MOUSEEVENTF_RIGHTUP = 0x0010
MOUSEEVENTF_MIDDLEDOWN = 0x0020
MOUSEEVENTF_MIDDLEUP = 0x0040
MOUSEEVENTF_VIRTUALDESK = 0x4000
MOUSEEVENTF_WHEEL = 0x0800
MOUSEEVENTF_XDOWN = 0x0080
MOUSEEVENTF_XUP = 0x0100


def MouseInput(flags, x, y, data):
    return MOUSEINPUT(x, y, data, flags, 0, None)


def Mouse(flags, x=0, y=0, data=0):
    return emul.Input(MouseInput(flags, x, y, data))


def left_click(x, y, delay = emul._DEFAULT_INPUT_TIMEOUT):
    x, y = screen.convert_physical_to_logical(x, y)
    emul.SendInput(Mouse(MOUSEEVENTF_MOVE | MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_LEFTDOWN, x, y),
                   Mouse(MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_LEFTUP, x, y),
                   delay=delay)


def move_to(x, y, delay = emul._DEFAULT_INPUT_TIMEOUT):
    print x, y
    x, y = screen.convert_physical_to_logical(x, y)
    print x, y
    emul.SendInput(Mouse(MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_MOVE, x, y), delay=delay)
