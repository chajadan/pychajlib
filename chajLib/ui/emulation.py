# python imports
import ctypes
import time
# custom imports
import keyboard_types
import mouse_types

_DEFAULT_INPUT_TIMEOUT = 0.2

LONG = ctypes.c_long
DWORD = ctypes.c_ulong
ULONG_PTR = ctypes.POINTER(DWORD)
WORD = ctypes.c_ushort

INPUT_MOUSE = 0
INPUT_KEYBOARD = 1
INPUT_HARDWARE = 2

class HARDWAREINPUT(ctypes.Structure):
    _fields_ = (('uMsg', DWORD),
                ('wParamL', WORD),
                ('wParamH', WORD))

class _INPUTunion(ctypes.Union):
    _fields_ = (('mi', mouse_types.MOUSEINPUT),
                ('ki', keyboard_types.KEYBDINPUT),
                ('hi', HARDWAREINPUT))

class INPUT(ctypes.Structure):
    _fields_ = (('type', DWORD),
                ('union', _INPUTunion))


def do_input_wait(delay = _DEFAULT_INPUT_TIMEOUT):
    time.sleep(delay)


def HardwareInput(message, parameter):
    return HARDWAREINPUT(message & 0xFFFFFFFF,
                         parameter & 0xFFFF,
                         parameter >> 16 & 0xFFFF)


def Input(structure):
    if isinstance(structure, mouse_types.MOUSEINPUT):
        return INPUT(INPUT_MOUSE, _INPUTunion(mi=structure))
    if isinstance(structure, keyboard_types.KEYBDINPUT):
        return INPUT(INPUT_KEYBOARD, _INPUTunion(ki=structure))
    if isinstance(structure, HARDWAREINPUT):
        return INPUT(INPUT_HARDWARE, _INPUTunion(hi=structure))
    raise TypeError('Cannot create INPUT structure!')


def Hardware(message, parameter=0):
    return Input(HardwareInput(message, parameter))


def SendInput(*inputs, **kwargs):
    """kwargs accepts an integer named delay, the represents seconds of timeout after input"""
    delay = kwargs.get("delay", _DEFAULT_INPUT_TIMEOUT)
    nInputs = len(inputs)
    LPINPUT = INPUT * nInputs
    pInputs = LPINPUT(*inputs)
    cbSize = ctypes.c_int(ctypes.sizeof(INPUT))
    inputs_sent = ctypes.windll.user32.SendInput(nInputs, pInputs, cbSize)
    do_input_wait(delay = delay)
    return inputs_sent