from dragonfly import Clipboard
import chajLib.ui.keyboard as kb
import chajLib.ui.clipboard as clipboard
from enum import Enum

SelectionCaretEffect = Enum("SelectionCaretEffect", "CaretRelative_Shifting SelectionRelative_NoShift")
class SelectionCaretEffect(Enum):
    Unknown = 0
    CaretRelative_Shifting = 1
    CaretRelative_NoShift = 2
    SelectionRelative_NoShift = 3


def capitalize_first_word_of_selection():
    selection = read_selection()
    kb.SendString(selection.capitalize())


def caret_after_left(target, sensitive = False):    
    copied = read_line_cursor_left()
    if not sensitive:
        copied = copied.lower()
        target = target.lower()
    places = copied.rfind(target)
    if places != -1:
        times = len(copied) - places - len(target)
        kb.sendLeft(times=times, delay=0)


def caret_after_right(target, sensitive = False):
    copied = read_line_cursor_right()
    if not sensitive:
        copied = copied.lower()
        target = target.lower()
    places = copied.find(target)
    if places != -1:
        times = places + len(target)
        kb.sendRight(times=times, delay=0)


def caret_before_left(target, sensitive=False):
    copied = read_line_cursor_left()
    if not sensitive:
        copied = copied.lower()
        target = target.lower()
    places = copied.rfind(target)
    if places != -1:
        times = len(copied) - places
        kb.sendLeft(times=times, delay=0)


def caret_before_right(target, sensitive=False):
    copied = read_line_cursor_right()
    if not sensitive:
        copied = copied.lower()
        target = target.lower()
    places = copied.find(target)
    if places != -1:
        kb.sendRight(times=places, delay=0)


def lowercase_first_word_of_selection():
    selection = read_selection()
    length = len(selection)
    if length > 0:
        selection = selection[0].lower() + selection[1:]
        kb.SendString(selection)


def read_selection():
    backup = Clipboard(from_system = True)
    kb.copy()
    selection = clipboard.get_clipboard_as_text()
    backup.copy_to_system()
    return selection


def read_line_cursor_right():
    """
    intended post-condition: caret position unaltered by call
    """
    kb.sendShiftEnd()
    lineEnd = read_selection()
    kb.sendLeft(delay=0)
    kb.sendEnd(delay=0)
    kb.sendLeft(len(lineEnd), delay=0)        
    return lineEnd


def read_line_cursor_left():
    """
    intended post-condition: caret position unaltered by call
    """
    kb.sendShiftHome()
    lineBegin = read_selection()
    kb.sendRight(delay=0)
    kb.sendHome(delay=0)
    kb.sendRight(len(lineBegin), delay=0)
    return lineBegin


def replace_all_in_line(to_replace, replacement, sensitive=False):
    select_line()
    replace_all_in_selection(to_replace, replacement, sensitive)


def replace_all_in_selection(to_replace, replacement, sensitive=False):
    selection = read_selection()
    if not sensitive:
        to_replace = to_replace.lower()
    kb.SendString(selection.replace(to_replace, replacement))


def replace_left(replacement):
    select_left()
    kb.SendString(replacement)


def replace_left_from_clipboard():
    select_left()
    kb.paste()


def replace_right(replacement):
    select_right()
    kb.SendString(replacement)


def replace_right_from_clipboard():
    select_right()
    kb.paste()


def respace_around_caret(count):
    """
    removes any surrounding spaces, if any, and inserts the requested number of space characters
    """
    leftline = read_line_cursor_left()
    rightline = read_line_cursor_right()
    leftSpaceCount = len(leftline) - len(leftline.rstrip())
    rightSpaceCount = len(rightline) - len(rightline.lstrip())
    kb.sendLeft(leftSpaceCount, delay=0)
    kb.sendShiftRight(leftSpaceCount + rightSpaceCount, delay=0)
    kb.SendString(" " * count, delay=0)
        
def select_characters_left(times):
    kb.shiftDown()
    kb.sendLeft(times = times, extended = True, delay = 0)
    kb.shiftUp()


def select_characters_right(times):
    kb.shiftDown()
    kb.sendRight(times = times, extended = True, delay = 0)
    kb.shiftUp()


def select_line():
    kb.sendEnd()
    kb.sendShiftHome()


def select_left():
    kb.sendShiftHome()


def select_right():
    kb.sendShiftEnd()


def select_through_left(target, sensitive = False):
    copied = read_line_cursor_left()
    if not sensitive:
        target = target.lower()
        copied = copied.lower()
    places = copied.rfind(target)
    if places != -1:
        select_characters_left(len(copied) - places)


def select_through_right(target, sensitive = False):
    copied = read_line_cursor_right()
    if not sensitive:
        target = target.lower()
        copied = copied.lower()
    places = copied.find(target)
    if places != -1:
        select_characters_right(places + len(target))
