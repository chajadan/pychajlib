import win32api

def get_screen_width_height():
    SM_CXSCREEN = 0
    SM_CYSCREEN = 1
    screen_width = win32api.GetSystemMetrics(SM_CXSCREEN)
    screen_height = win32api.GetSystemMetrics(SM_CYSCREEN)
    return screen_width, screen_height    


def convert_physical_to_logical(x, y):
    screen_width, screen_height = get_screen_width_height()
    return x * 65535 / screen_width, y * 65535 / screen_height