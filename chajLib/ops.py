"""
Contains helper function, generally seen as 'basic operations',
usually condensed or made more explicit through naming"""


def first_not_none(*args):
    for arg in args:
        if arg is not None:
            return arg
    return None