def split_on_any_char(to_split, iter_chars):
    parts = []
    begin_index = -1
    for i, chr in enumerate(to_split):
        if chr in iter_chars and begin_index != -1:
            parts.append(to_split[begin_index:i])
            begin_index = -1
        elif chr in iter_chars and begin_index == -1:
            continue
        elif chr not in iter_chars and begin_index == -1:
            begin_index = i
    if begin_index != -1:
        parts.append(to_split[begin_index:])
    return parts