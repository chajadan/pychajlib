import codecs
import datetime
import os
import shutil
import subprocess


def copy_file(current_path, new_path):
    shutil.copy2(current_path, new_path)


def filenames_by_prefix(search_directory, filename_prefix):
    filenames = []
    for directory_item in os.listdir(search_directory):
        if os.path.isfile(os.path.join(search_directory, directory_item)) and directory_item.startswith(filename_prefix):
            filenames.append(directory_item)
    return filenames


def get_file_string(filepath, encoding="ascii", errors="ignore"):
    if not os.path.isfile(filepath):
        return None

    file = codecs.open(filepath, "r", encoding=encoding, errors=errors)
    if file.closed == False:
        try:
            filestring = file.read()
            file.close()
            return filestring
        except Exception as e:
            print e
            return None
    else:
        return None


def get_modification_dates(filepath_list):
    mod_dates = []
    for filepath in filepath_list:
        mod_dates.append(modification_date(filepath))
    return mod_dates


def gui_open_folder(path=None):
    command = ["explorer.exe"]
    if path:
        if not os.path.isdir(path):
            path = os.path.dirname(path)
        if os.path.isdir(path):
            command.append(path)
    subprocess.call(command)
    

def modification_date(filepath):
    t = os.path.getmtime(filepath)
    return datetime.datetime.fromtimestamp(t)


def move_file(current_path, new_path):
    shutil.move(current_path, new_path)

def newest_filepath_with_prefix(search_directory, filename_prefix):
    filepaths = [os.path.join(search_directory, filename) for filename in filenames_by_prefix(search_directory, filename_prefix)]
    filetimes = get_modification_dates(filepaths)
    zipped = zip(filetimes, filepaths)
    zipped.sort()
    try:
        return zipped[-1][1]
    except:
        return None
 
 
def write_file_string(filepath, filestring, encoding="ascii", errors="ignore"):
    file = codecs.open(filepath, "w", encoding=encoding, errors=errors)
    if not file.closed:
    	try:
        	file.write(filestring)
         	file.close()
        except Exception as e:
        	print e
        	return False
        return True
    else:
        return False