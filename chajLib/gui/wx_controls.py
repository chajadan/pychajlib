"""wxPython custom controls"""

import wx
import wx.lib.scrolledpanel as scrolled
import chajLib.file_system as fs
from chajLib.gui.wx_validators import WriteFileValidator
from __builtin__ import True

class ChoiceByPrefix(wx.Choice):
    """ A wx.Choice that selects the item typed at the keyboard """
    def __init__(self, parent, id = -1, size = wx.DefaultSize, choices = [], validator = None):
        if validator is None:
            wx.Choice.__init__(self, parent, id, size = size, choices = choices)
        else:
            wx.Choice.__init__(self, parent, id, size = size, choices = choices, validator = validator)
        self.Bind(wx.EVT_KEY_DOWN, self.OnKey)
        self.typedText = ""

    def FindPrefix(self, prefix):
        if prefix:
            prefix = prefix.lower()
            length = len(prefix)

            for x in range(self.GetCount()):
                text = self.GetString(x)
                text = text.lower()

                if text[:length] == prefix:
                    return x
        return -1

    def OnKey(self, evt):
        key = evt.GetKeyCode()

        if key >= 32 and key <= 127:
            self.typedText = self.typedText + chr(key)
            item = self.FindPrefix(self.typedText)

            if item != -1:
                self.SetSelection(item)

        elif key == wx.WXK_BACK:   # backspace removes one character and backs up
            self.typedText = self.typedText[:-1]

            if not self.typedText:
                self.SetSelection(0)
            else:
                item = self.FindPrefix(self.typedText)

                if item != -1:
                    self.SetSelection(item)
        else:
            self.typedText = ''
            evt.Skip()

    def OnKeyDown(self, evt):
        pass

class ChoiceWithOther(ChoiceByPrefix):
    """A wx.Choice with a special entry that allows temprorary additions of new options"""
    def __init__(self, parent, id, choices = [], otherChoiceString = "Other...", default = None, sorted = True, validator = None):
        self.choices = [] + choices
        if otherChoiceString in self.choices:
            self.choices.remove(otherChoiceString)
        self.sorted = sorted
        if self.sorted:
            self.choices.sort()
        if validator is None:
            ChoiceByPrefix.__init__(self, parent, id=id, choices=self.choices)
            #wx.Choice.__init__(self, parent, id, choices = self.choices)
        else:
            ChoiceByPrefix.__init__(self, parent, id=id, choices=self.choices, validator=validator)
            #wx.Choice.__init__(self, parent, id, choices = self.choices, validator = validator)
        self.otherChoiceString = otherChoiceString
        self.AddOtherChoiceEntry()
        if default:
            self.SetStringSelection(default)
        else:
            self.SetStringSelection(self.choices[0])
        self.Bind(wx.EVT_CHOICE, self.OnChoose)

    def OnChoose(self, event):
        if self.GetStringSelection() == self.otherChoiceString:
            dlg = wx.TextEntryDialog(self, 'What new choice do you want?',
                'New Choice')
            if dlg.ShowModal() == wx.ID_OK:
                self.DoAddNewChoice(dlg.GetValue())
            dlg.Destroy()
        event.Skip()

    def DoAddNewChoice(self, newChoice):
        if newChoice not in self.choices:
            self.choices.append(newChoice)
            self.Clear()
            if self.sorted:
                self.choices.sort()
            self.AppendItems(self.choices)
            self.AddOtherChoiceEntry()
            self.SetStringSelection(newChoice)
        else:
            wx.MessageBox("That choice already exists.", "Choice Exists")

    def AddOtherChoiceEntry(self):
            self.Append(self.otherChoiceString)

class ScrolledPanel_FixedDimension(scrolled.ScrolledPanel):
    def __init__(self, parent, id, fixed_dimension):
        scrolled.ScrolledPanel.__init__(self, parent, id)
        self.fixed_dimension = fixed_dimension
        
    def SetSizer_FixDimension(self, sizer):
        self.SetSizerAndFit(sizer)
        baseSize = self.GetSize()
        if self.fixed_dimension == wx.VERTICAL:
            scrollBarDimension = wx.SYS_HSCROLL_Y
        elif self.fixed_dimension == wx.HORIZONTAL:
            scrollBarDimension = wx.SYS_VSCROLL_X
        else:
            if type(self.fixed_dimension) == type(wx.VERTICAL):
                raise ValueError
            else:
                raise TypeError
                    
        scrollbarSize = wx.SystemSettings_GetMetric(scrollBarDimension)
        
        if self.fixed_dimension == wx.VERTICAL:
            minHeight = baseSize + scrollbarSize
            self.SetSizeHints(-1, minHeight, -1, -1)
        elif self.fixed_dimension == wx.HORIZONTAL:
            minWidth = baseSize.width + scrollbarSize
            self.SetSizeHints(minWidth, -1, -1, -1)
    
class ScrolledPanel_FixedWidth(ScrolledPanel_FixedDimension):
    def __init__(self, parent, id = -1):
        ScrolledPanel_FixedDimension.__init__(self, parent, id, wx.HORIZONTAL)
        
class ScrolledPanel_FixedHeight(ScrolledPanel_FixedDimension):
    def __init__(self, parent, id = -1):
        ScrolledPanel_FixedDimension.__init__(self, parent, id, wx.VERTICAL)

class LabeledBoxSizer(wx.StaticBoxSizer):
    """ Basically a short cut to creating a static box and it's related sizer """
    def __init__(self, parent, id = -1, labelText = "", direction = wx.VERTICAL):
         wx.StaticBoxSizer.__init__(self, wx.StaticBox(parent, -1, labelText), direction)

class LabeledCtrl(wx.Panel):
    def __init__(self, parent, id, ctrl, labelText = ""):
        wx.Panel.__init__(self, parent, id)
        self.SetExtraStyle(wx.WS_EX_VALIDATE_RECURSIVELY)
        self.label = wx.StaticText(self, -1, labelText, style = wx.ALIGN_CENTER_VERTICAL)
        self.ctrl = ctrl
        self.ctrl.Reparent(self)

        self.sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.sizer.Add(self.label, flag = wx.ALL | wx.ALIGN_CENTER_VERTICAL, border = 5)
        self.sizer.Add(self.ctrl, flag = wx.ALL | wx.ALIGN_CENTER_VERTICAL, border = 5)
        self.SetSizer(self.sizer)
         
class PaddingSizer(wx.BoxSizer):
    """ Most commonly used to hold a single control that simple needs some margins to breath """
    """ Most common use case:
            x = ... # some widget, often a sizer or frame
            paddingSizer = PaddingSizer(x)
    """
    def __init__(self, ctrl = None, direction = wx.VERTICAL, proportion = 1, flag = wx.EXPAND|wx.ALL, border = 5):
        wx.BoxSizer.__init__(self, direction)
        if ctrl:
            self.Add(ctrl, proportion, flag = flag, border = border)
    
class StaticBoxedCtrl(LabeledBoxSizer):
    def __init__(self, ctrl, labelText="", proportion=1, borderFlags=wx.ALL, border=5):
        LabeledBoxSizer.__init__(self, ctrl.GetParent(), labelText=labelText)
        self.Add(ctrl, proportion, flag=wx.EXPAND|borderFlags, border=border)
        #self.Add(ctrl, proportion, flag=borderFlags, border=border)

class TextCtrl_Multiline(wx.TextCtrl):
    def __init__(self, parent, process_tab =True, **kwargs):
        kwargs["style"] = kwargs.get("style", 0) | wx.TE_MULTILINE
        if process_tab:
            kwargs["style"] = kwargs.get("style", 0) | wx.TE_PROCESS_TAB
        wx.TextCtrl.__init__(self, parent, **kwargs)


class TextFilePanel(wx.Panel):
    def __init__(self, parent, filepath, id=-1):
        wx.Panel.__init__(self, parent, id=id)
        self.filepath = filepath
        self.textview = TextCtrl_Multiline(self)
        self.SetValidator(WriteFileValidator(self.filepath, self.textview.GetValue))
        
        
        contentString = fs.get_file_string(self.filepath)
        if contentString:
            self.textview.SetValue(contentString)
        
        self.sizer = wx.BoxSizer()
        self.sizer.Add(self.textview, 1, flag=wx.EXPAND)
        self.SetSizer(self.sizer)
    
class TightStaticBoxedCtrl(LabeledBoxSizer):
    def __init__(self, ctrl, labelText = "", proportion = 0, flag = wx.ALL, border = 0):
        LabeledBoxSizer.__init__(self, ctrl.GetParent(), labelText = labelText)
        self.Add(ctrl, proportion, flag = flag, border = border)

class ValidatorPanel(wx.Panel):
    """ A shortcut to create children panels that will continue any recursive validation that comes their way """
    def __init__(self, parent, id = -1):
        wx.Panel.__init__(self, parent, id)
        self.SetExtraStyle(wx.WS_EX_VALIDATE_RECURSIVELY)
