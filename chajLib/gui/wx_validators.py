import wx
import chajLib.file_system as fs

class WriteFileValidator(wx.PyValidator):
    def __init__(self, filepath, content_callback):
        self.filepath = filepath
        self.content_callback = content_callback
        wx.PyValidator.__init__(self)

    def Clone(self):
        return WriteFileValidator(self.filepath, self.content_callback)

    def Validate(self, win):
        return True
    
    def TransferFromWindow(self):
        content = self.content_callback()
        try:
            content = content.encode('ascii', 'ignore')
        except Exception as e:
            print e
            return False
        return fs.write_file_string(self.filepath, content)
    
    def TransferToWindow(self):
        return True  